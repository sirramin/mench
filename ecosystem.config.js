module.exports = {
  "apps" : [
      {
          "name": "platform-Master",
          "script": "index.js",
          "args": ["platform-Master"],
          "instances": "1",
          "combine_logs": true,
          "exec_mode" : "cluster"
      },
      {
          "name": "platform-Slave",
          "script": "index.js",
          "args": ["platform-Slave"],
          "instances": "6",
          "combine_logs": true,
          "exec_mode" : "cluster"
      }
  ]
}